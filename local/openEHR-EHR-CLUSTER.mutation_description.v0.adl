archetype (adl_version=1.4; uid=2de96348-c24b-4951-81fd-ae8898701a34)
	openEHR-EHR-CLUSTER.mutation_description.v0

concept
	[at0000]

language
	original_language = <[ISO_639-1::en]>

description
	original_author = <
		["date"] = <"2019-03-19">
	>
	lifecycle_state = <"unmanaged">
	details = <
		["en"] = <
			language = <[ISO_639-1::en]>
		>
	>
	other_details = <
		["licence"] = <"This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/.">
		["custodian_organisation"] = <"openEHR Foundation">
		["original_namespace"] = <"org.openehr">
		["original_publisher"] = <"openEHR Foundation">
		["custodian_namespace"] = <"org.openehr">
		["MD5-CAM-1.0.1"] = <"d3571ffb88c046535143c7bebf03ddc1">
		["build_uid"] = <"b010a2dc-8eb8-3bd7-afa4-eb7bb070d6fa">
	>

definition
	CLUSTER[at0000] matches {    -- Mutation description
		items cardinality matches {1..*; unordered} matches {
			allow_archetype CLUSTER[at0014] occurrences matches {0..*} matches {    -- Reference sequence
				include
					archetype_id/value matches {/.*|openEHR-EHR-CLUSTER\.Reference_sequence\.v0/}
			}
			ELEMENT[at0016] occurrences matches {0..1} matches {    -- Mutation type
				value matches {
					DV_CODED_TEXT matches {
						defining_code matches {
							[local::
							at0019,    -- Deletion
							at0020,    -- Duplication
							at0021,    -- Copy number
							at0022,    -- Insertion
							at0023,    -- Inversion
							at0024,    -- Conversion
							at0025,    -- Indel
							at0026,    -- Repeated
							at0101]    -- Substitution
						}
					}
				}
			}
			CLUSTER[at0018] occurrences matches {0..2} matches {    -- Reference nucleotide(s)
				items cardinality matches {1..*; unordered} matches {
					ELEMENT[at0013] occurrences matches {0..1} matches {    -- Nucleotides range
						value matches {
							DV_INTERVAL<DV_COUNT> matches {*}
						}
					}
					ELEMENT[at0015] occurrences matches {0..1} matches {    -- Reference nucleotide(s) string
						value matches {
							DV_TEXT matches {*}
						}
					}
				}
			}
			CLUSTER[at0093] occurrences matches {0..2} matches {    -- New nucleotide(s)
				items cardinality matches {1..*; unordered} matches {
					ELEMENT[at0094] occurrences matches {0..1} matches {    -- Nucleotides range
						value matches {
							DV_INTERVAL<DV_COUNT> matches {*}
						}
					}
					ELEMENT[at0095] occurrences matches {0..1} matches {    -- Reference nucleotide(s) string
						value matches {
							DV_TEXT matches {*}
						}
					}
				}
			}
			ELEMENT[at0027] occurrences matches {0..1} matches {    -- Total copy number
				value matches {
					DV_COUNT matches {*}
				}
			}
			ELEMENT[at0030] occurrences matches {0..1} matches {    -- Copy number type
				value matches {
					DV_CODED_TEXT matches {
						defining_code matches {
							[local::
							at0031,    -- Gain
							at0032]    -- Loss
						}
					}
				}
			}
		}
	}

ontology
	term_definitions = <
		["en"] = <
			items = <
				["at0000"] = <
					text = <"Mutation description">
					description = <"Variant description">
				>
				["at0013"] = <
					text = <"Nucleotides range">
					description = <"*">
				>
				["at0014"] = <
					text = <"Reference sequence">
					description = <"*">
				>
				["at0015"] = <
					text = <"Reference nucleotide(s) string">
					description = <"*">
				>
				["at0016"] = <
					text = <"Mutation type">
					description = <"*">
				>
				["at0018"] = <
					text = <"Reference nucleotide(s)">
					description = <"*">
				>
				["at0019"] = <
					text = <"Deletion">
					description = <"*">
				>
				["at0020"] = <
					text = <"Duplication">
					description = <"*">
				>
				["at0021"] = <
					text = <"Copy number">
					description = <"*">
				>
				["at0022"] = <
					text = <"Insertion">
					description = <"*">
				>
				["at0023"] = <
					text = <"Inversion">
					description = <"*">
				>
				["at0024"] = <
					text = <"Conversion">
					description = <"*">
				>
				["at0025"] = <
					text = <"Indel">
					description = <"*">
				>
				["at0026"] = <
					text = <"Repeated">
					description = <"*">
				>
				["at0027"] = <
					text = <"Total copy number">
					description = <"*">
				>
				["at0030"] = <
					text = <"Copy number type">
					description = <"*">
				>
				["at0031"] = <
					text = <"Gain">
					description = <"*">
				>
				["at0032"] = <
					text = <"Loss">
					description = <"*">
				>
				["at0093"] = <
					text = <"New nucleotide(s)">
					description = <"*">
				>
				["at0094"] = <
					text = <"Nucleotides range">
					description = <"*">
				>
				["at0095"] = <
					text = <"Reference nucleotide(s) string">
					description = <"*">
				>
				["at0101"] = <
					text = <"Substitution">
					description = <"*">
				>
			>
		>
	>
