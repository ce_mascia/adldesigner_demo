archetype (adl_version=1.4; uid=e0ec5eb0-4869-492d-9bd8-e93bfd496a0f)
	openEHR-EHR-CLUSTER.intraductal_carcinoma_presence.v0

concept
	[at0000]

language
	original_language = <[ISO_639-1::en]>

description
	original_author = <
		["date"] = <"2020-04-02">
		["name"] = <"Peer">
	>
	lifecycle_state = <"unmanaged">
	details = <
		["en"] = <
			language = <[ISO_639-1::en]>
		>
	>
	other_details = <
		["licence"] = <"This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.">
		["custodian_organisation"] = <"openEHR Foundation">
		["original_namespace"] = <"org.openehr">
		["original_publisher"] = <"openEHR Foundation">
		["custodian_namespace"] = <"org.openehr">
		["MD5-CAM-1.0.1"] = <"90397f9a515cf3ed59df4bdfa0b492b5">
		["build_uid"] = <"fe48849f-49ac-33c5-8ad8-d7c0f41e28de">
	>

definition
	CLUSTER[at0000] matches {    -- Intraductal Carcinoma Presence
		items cardinality matches {1..*; unordered} matches {
			ELEMENT[at0001] occurrences matches {0..1} matches {    -- Invasion
				value matches {
					DV_CODED_TEXT matches {
						defining_code matches {
							[local::
							at0002,    -- Not identified
							at0003,    -- Present
							at0004]    -- Cannot be determined
						}
					}
				}
			}
		}
	}

ontology
	terminologies_available = <"NCIT", ...>
	term_definitions = <
		["en"] = <
			items = <
				["at0000"] = <
					text = <"Intraductal Carcinoma Presence">
					description = <"Intraductal Carcinoma Presence">
				>
				["at0001"] = <
					text = <"Invasion">
					description = <"*">
				>
				["at0002"] = <
					text = <"Not identified">
					description = <"The finding is not present, but a value of 'none' cannot be unequivocally established absence in the current context.">
				>
				["at0003"] = <
					text = <"Present">
					description = <"There is evidence of intraductal carcinoma presence">
				>
				["at0004"] = <
					text = <"Cannot be determined">
					description = <"Evidence of local invasion by tumour has not been determined">
				>
			>
		>
	>
	term_bindings = <
		["NCIT"] = <
			items = <
				["at0004"] = <[NCIT(20.03e)::C48658]>
				["at0003"] = <[NCIT(20.03e)::C25626]>
				["at0002"] = <[NCIT(20.03e)::C48661]>
			>
		>
	>
