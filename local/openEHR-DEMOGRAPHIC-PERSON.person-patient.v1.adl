archetype (adl_version=1.4; uid=bf51ac66-7568-3203-9be2-edbe544e728d)
	openEHR-DEMOGRAPHIC-PERSON.person-patient.v1
specialize
	openEHR-DEMOGRAPHIC-PERSON.person.v1

concept
	[at0000.1]

language
	original_language = <[ISO_639-1::pt-br]>
	translations = <
		["en"] = <
			language = <[ISO_639-1::en]>
			author = <
				["name"] = <"Sergio Miranda Freire">
				["organisation"] = <"Universidade do Estado do Rio de Janeiro - UERJ">
				["email"] = <"sergio@lampada.uerj.br">
			>
		>
	>

description
	original_author = <
		["date"] = <"22/05/2009">
		["name"] = <"Sergio Miranda Freire & Rigoleta Dutra Mediano Dias">
		["organisation"] = <"Universidade do Estado do Rio de Janeiro - UERJ">
		["email"] = <"sergio@lampada.uerj.br">
	>
	lifecycle_state = <"Authordraft">
	details = <
		["pt-br"] = <
			language = <[ISO_639-1::pt-br]>
			purpose = <"Representação dos dados demográficos de um paciente.">
			keywords = <"serviço demográfico","dados de um paciente">
			copyright = <"© 2011 openEHR Foundation">
			use = <"Usado em serviço demográficos para coletar os dados de um paciente.">
		>
		["en"] = <
			language = <[ISO_639-1::en]>
			purpose = <"Representation of a patient's demographic data.">
			keywords = <"demographic service","patient's data">
			copyright = <"© 2011 openEHR Foundation">
			use = <"Used in demographic service to collect a patient's data.">
		>
	>
	other_details = <
		["references"] = <"ISO/TS 22220:2008(E) - Identification of Subject of Care - Technical Specification - International Organization for Standardization.">
		["MD5-CAM-1.0.1"] = <"f74bb12704664d032bba589ff9133eb0">
		["build_uid"] = <"1c60d209-3e39-3251-83d0-8a2802d10d05">
	>

definition
	PERSON[at0000.1] matches {    -- Dados do paciente
		details matches {
			allow_archetype ITEM_TREE[at0001] matches {    -- Detalhes
				include
					archetype_id/value matches {/(person_details)[a-zA-Z0-9_-]*\.v1/}
			}
		}
		identities cardinality matches {1..*; unordered} matches {
			allow_archetype PARTY_IDENTITY[at0002.1] matches {    -- Nome
				include
					archetype_id/value matches {/(person_name)[a-zA-Z0-9_-]*\.v1/}
			}
		}
		contacts cardinality matches {0..*; unordered} matches {
			CONTACT[at0003.1] matches {    -- Contatos
				addresses cardinality matches {1..*; unordered} matches {
					allow_archetype ADDRESS[at0030] matches {    -- Endereço
						include
							archetype_id/value matches {/(electronic_communication)[a-zA-Z0-9_-]*\.v1/}
							archetype_id/value matches {/(address)([a-zA-Z0-9_]+)*\.v1/}
					}
				}
			}
		}
		relationships cardinality matches {0..*; unordered} matches {
			PARTY_RELATIONSHIP[at0004.1] matches {    -- Relacionamentos
				details matches {
					ITEM_TREE[at0.40] matches {    -- Personal relationships
						items cardinality matches {0..*; unordered} matches {
							ELEMENT[at0040] matches {    -- Grau de parentesco
								value matches {
									DV_TEXT matches {*}
									DV_CODED_TEXT matches {
										defining_code matches {
											[ac0000]
										}
									}
								}
							}
						}
					}
				}
			}
			PARTY_RELATIONSHIP[at0.2] matches {    -- Fonte pagadora
				details matches {
					ITEM_TREE[at0.20] matches {    -- Identificações do beneficiário
						items cardinality matches {0..*; unordered} matches {
							allow_archetype CLUSTER[at0.21] matches {    -- Identificação do beneficiário
								include
									archetype_id/value matches {/(person_identifier)[a-zA-Z0-9_-]*\.v1/}
							}
						}
					}
				}
			}
			PARTY_RELATIONSHIP[at0.3] matches {    -- Serviço de saúde/Profissional de saúde
				details matches {
					ITEM_TREE[at0.30] matches {    -- Identificações no prestador
						items cardinality matches {0..*; unordered} matches {
							allow_archetype CLUSTER[at0.31] matches {    -- Identificação no prestador
								include
									archetype_id/value matches {/(person_identifier)[a-zA-Z0-9_-]*\.v1/}
							}
						}
					}
				}
			}
		}
	}

ontology
	term_definitions = <
		["pt-br"] = <
			items = <
				["at0000.1"] = <
					text = <"Dados do paciente">
					description = <"Dados do paciente.">
				>
				["at0001"] = <
					text = <"Detalhes">
					description = <"Detalhes demográficos do paciente.">
				>
				["at0002"] = <
					text = <"Nome">
					description = <"Conjunto de dados que especificam o nome da pessoa.">
				>
				["at0003"] = <
					text = <"Contatos">
					description = <"Contatos da pessoa.">
				>
				["at0004"] = <
					text = <"Relacionamentos">
					description = <"Relacionamentos de uma pessoa, especialmente laços familiares.">
				>
				["at0002.1"] = <
					text = <"Nome">
					description = <"Conjunto de dados que especificam o nome do paciente.">
				>
				["at0003.1"] = <
					text = <"Contatos">
					description = <"Contatos da pessoa.">
				>
				["at0004.1"] = <
					text = <"Relacionamentos">
					description = <"Relacionamentos de um paciente, especialmente laços familiares.">
				>
				["at0030"] = <
					text = <"Endereço">
					description = <"Endereços vinculados a um único contato, ou seja, com o mesmo período de validade.">
				>
				["at0040"] = <
					text = <"Grau de parentesco">
					description = <"Define o grau de parentesco entre as pessoas envolvidas.">
				>
				["at0.2"] = <
					text = <"Fonte pagadora">
					description = <"Beneficiário: Relacionamento do paciente com uma Fonte Pagadora.">
				>
				["at0.3"] = <
					text = <"Serviço de saúde/Profissional de saúde">
					description = <"Relacionamento do paciente com uma organização prestadora de serviço de saúde/profissional de saúde.">
				>
				["at0.40"] = <
					text = <"Personal relationships">
					description = <"Personal relationship.">
				>
				["at0.20"] = <
					text = <"Identificações do beneficiário">
					description = <"Identificações do beneficiário junto à fonte pagadora.">
				>
				["at0.21"] = <
					text = <"Identificação do beneficiário">
					description = <"Documento de identificação do beneficiário junto à fonte pagadora.">
				>
				["at0.30"] = <
					text = <"Identificações no prestador">
					description = <"Identificações do paciente junto ao prestador.">
				>
				["at0.31"] = <
					text = <"Identificação no prestador">
					description = <"Documento de identificação do paciente junto ao prestador.">
				>
			>
		>
		["en"] = <
			items = <
				["at0000.1"] = <
					text = <"Patient">
					description = <"Patient demographic data.">
				>
				["at0001"] = <
					text = <"Demographic details">
					description = <"A patient's demographic details.">
				>
				["at0002"] = <
					text = <"Name">
					description = <"A person's name.">
				>
				["at0003"] = <
					text = <"Contacts">
					description = <"A person's contacts.">
				>
				["at0004"] = <
					text = <"Relationships">
					description = <"A person's relationships, especially family ties.">
				>
				["at0002.1"] = <
					text = <"Name">
					description = <"A patient's name.">
				>
				["at0003.1"] = <
					text = <"Contacts">
					description = <"A patient's contacts.">
				>
				["at0004.1"] = <
					text = <"Relationships">
					description = <"A patient's relationships, especially family ties.">
				>
				["at0030"] = <
					text = <"Addresses">
					description = <"Addresses linked to a single contact, i.e. with the same time validity.">
				>
				["at0040"] = <
					text = <"Relationship type">
					description = <"Defines the type of relationship between related persons.">
				>
				["at0.2"] = <
					text = <"Third party payer">
					description = <"Relationship between the patient and a third-party payer.">
				>
				["at0.3"] = <
					text = <"Healthcare provider/Health professional">
					description = <"Patient: relationship between the patient and a healthcare provider organisation/health professional.">
				>
				["at0.40"] = <
					text = <"Personal relationship">
					description = <"Personal relationship.">
				>
				["at0.20"] = <
					text = <"Patient identifiers">
					description = <"Identifiers of the patient at the third-party payer.">
				>
				["at0.21"] = <
					text = <"Healthcare consumer identifier.">
					description = <"An identifier of the patient at the third-party payer.">
				>
				["at0.30"] = <
					text = <"Patient identifiers">
					description = <"Patient identifiers at the related healthcare provider.">
				>
				["at0.31"] = <
					text = <"Patient identifier">
					description = <"A patient identifier at the related healthcare provider.">
				>
			>
		>
	>
	constraint_definitions = <
		["pt-br"] = <
			items = <
				["ac0000"] = <
					text = <"Códigos para tipo de parentesco">
					description = <"códigos válidos para tipo de parentesco.">
				>
			>
		>
		["en"] = <
			items = <
				["ac0000"] = <
					text = <"Codes for the type of relationship">
					description = <"Valid codes for the type of relationship.">
				>
			>
		>
	>
